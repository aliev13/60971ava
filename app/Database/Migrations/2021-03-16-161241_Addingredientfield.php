<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addingredientfield extends Migration
{
    public function up()
    {
        if ($this->db->tableexists('dish')) {
            $this->forge->addColumn('dish', array(
                'ingredient' => array('type' => 'TEXT', 'null' => FALSE),
            ));
        }
    }

    public function down()
    {
        $this->forge->dropColumn('dish', 'ingredient');
    }
}

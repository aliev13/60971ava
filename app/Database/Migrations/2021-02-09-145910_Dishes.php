<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Dishes extends Migration
{
	public function up()
	{
        if (!$this->db->tableexists('categories'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('categories', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('dish'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'cooking_method' => array('type' => 'TEXT', 'null' => FALSE),
                'cooking_time' => array('type' => 'REAL', 'null' => FALSE),
                'category_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => TRUE),
                'photo' => array('type' => 'TEXT', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('category_id','categories','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('dish', TRUE);
        }

        if (!$this->db->tableexists('ingredient'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'units' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('ingredient', TRUE);
        }

        if (!$this->db->tableexists('recipe'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'dish_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'ingredient_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'count' => array('type' => 'INT', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('dish_id','dish','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('ingredient_id','ingredient','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('recipe', TRUE);
        }
    }

	//--------------------------------------------------------------------

	public function down()
	{
        $this->forge->droptable('recipe');
        $this->forge->droptable('dish');
        $this->forge->droptable('ingredient');
        $this->forge->droptable('categories');
	}
}

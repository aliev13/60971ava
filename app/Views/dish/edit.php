<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<?php if (!empty($dish)) : ?>
    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('dish/update'); ?>
        <input type="hidden" name="id" value="<?= $dish["id"] ?>">

        <div class="form-group">
            <label for="name">Имя</label>
            <input id="name" type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
                   value="<?= $dish["name"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="cooking_method">Метод приготовления</label>
            <textarea rows="8" id="cooking_method" type="text" class="form-control <?= ($validation->hasError('cooking_method')) ? 'is-invalid' : ''; ?>" name="cooking_method"
                      ><?= $dish["cooking_method"]; ?></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('cooking_method') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="ingredient">Инридиенты</label>
            <textarea rows="8" id="ingredient" type="text" class="form-control <?= ($validation->hasError('ingredient')) ? 'is-invalid' : ''; ?>" name="ingredient"
            ><?= $dish["ingredient"]; ?></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('ingredient') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="cooking_time">Время приготовления (в минутах)</label>
            <input id="cooking_time" type="number" class="form-control <?= ($validation->hasError('cooking_time')) ? 'is-invalid' : ''; ?>" name="cooking_time"
                   value="<?= $dish["cooking_time"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('cooking_time') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="photo">Фото</label>
            <input id="cooking_time" type="text" class="form-control <?= ($validation->hasError('photo')) ? 'is-invalid' : ''; ?>" name="photo"
                   value="<?= $dish["photo"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('photo') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?php endif ?>
<?= $this->endSection() ?>
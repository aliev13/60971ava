<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container">
        <h2 class="text-center" style="font-weight: bold">Все блюда</h2>
        <div class="d-flex flex-wrap">

            <?php if (!empty($dish) && is_array($dish)) : ?>

                <?php foreach ($dish as $item): ?>
                    <div class="card m-4" style="width: 18rem;">
                        <img height="150" src="<?= esc($item['photo']); ?>" class="card-img-top" alt="<?= esc($item['name']); ?>">

                        <div class="card-body">
                            <h4 class="card-title" style="font-weight: bold"><?= esc($item['name']); ?></h4>
                            <h6>Способ приготовления:</h6>
                            <p class="card-text"><?= mb_strimwidth(esc($item['cooking_method']), 0, 100, "..."); ?></p>
                            <a href="<?= base_url()?>/index.php/dish/view/<?= esc($item['id']); ?>" class="btn btn-primary">Подробнее</a>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else : ?>
                <p>Невозможно найти блюда.</p>
            <?php endif ?>
        </div>
    </div>
<?= $this->endSection() ?>
<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container">
    <?php if (!empty($dish)) : ?>
    <div class="row justify-content-center" style="background-color: #EDEEF0; border-radius: 15px;">
        <div class="card mt-3">
            <img src="<?= esc($dish['photo']); ?>" class="img-fluid align-self-center img-one-dish" alt="Нет изображения">
        </div>
        <div class="col-12 text-center">
            <h3 style="font-weight: bold"><?= esc($dish['name']); ?></h3>
        </div>
        <div class="col-12 mt-5">
            <h3>Время приготовления: <span style="font-weight: bold"><?= esc($dish['cooking_time']); ?> минут</span></h3>
        </div>
       <div class="col-12 mt-3">
           <h3>Метод приготоления:</h3>
           <div class="form-floating">
               <textarea readonly class="form-control" style="height: 350px; background-color: #ffffff"><?= esc($dish['cooking_method']);?></textarea>
           </div>
           <h3 class="mt-3">Ингридиенты:</h3>
           <div class="form-floating mb-3">
               <textarea readonly class="form-control" style="height: 350px; background-color: #ffffff"><?= esc($dish['ingredient']);?></textarea>
           </div>
       </div>
    <?php else : ?>
        <p>Невозможно найти блюда.</p>
    <?php endif ?>
    </div>
    <div class="row justify-content-end mt-3">
        <div class="col-md-3 col-sm-12 col-12">
            <a class="btn btn-primary" href="<?= base_url()?>/index.php/dish/edit/<?= esc($dish['id']); ?>">Редактировать</a>
            <a class="btn btn-danger" href="<?= base_url()?>/index.php/dish/delete/<?= esc($dish['id']); ?>">Удалить</a>
        </div>
    </div>
</div>

<?= $this->endSection() ?>
<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">
        <?= form_open_multipart('dish/store'); ?>

        <div class="form-group">
            <label for="name">Название</label>
            <input id="name" type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="ingredient">Ингридиенты</label>
            <textarea rows="8" id="ingredient" type="text" class="form-control <?= ($validation->hasError('ingredient')) ? 'is-invalid' : ''; ?>" name="ingredient"></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('ingredient') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="cooking_method">Метод приготовления</label>
            <textarea rows="8" id="cooking_method" type="text" class="form-control <?= ($validation->hasError('cooking_method')) ? 'is-invalid' : ''; ?>" name="cooking_method"></textarea>
            <div class="invalid-feedback">
                <?= $validation->getError('cooking_method') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="cooking_time">Время приготовления (в минутах)</label>
            <input id="cooking_time" type="number" class="form-control <?= ($validation->hasError('cooking_time')) ? 'is-invalid' : ''; ?>" name="cooking_time">
            <div class="invalid-feedback">
                <?= $validation->getError('cooking_time') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="picture">Изображение</label>
            <input id="picture" type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>

<?= $this->endSection() ?>
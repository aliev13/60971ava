<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>/css/style.css">
    <title>Кулинарный справочник</title>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="<?php echo base_url();?>/index.php">Кулинарный справочник</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url();?>/index.php">Главная<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url()?>/index.php/dish">Блюда</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url()?>/dish/store">Создать блюдо</a>
            </li>
        </ul>
        <?php if (! $ionAuth->loggedIn()): ?>
            <div class="nav-item dropdown">
                <a class="nav-link active btn-primary" href="<?= base_url()?>/auth/login"><span class="fas fa fa-sign-in-alt" style="color:white"></span>Вход</a>
            </div>
        <?php else: ?>
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                    <?php echo $ionAuth->user()->row()->email; ?>
                </button>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="<?= base_url()?>/auth/logout">Выход</a></li>
                </ul>
            </div>
        <?php endif ?>
    </div>
</nav>
<div class="content pt-3">
    <main role="main">
        <?php if (session()->getFlashdata('message')) :?>
            <div class="alert alert-info mr-5 ml-5" role="alert">
                <?= session()->getFlashdata('message') ?>
            </div>
        <?php endif ?>

        <?= $this->renderSection('content') ?>
    </main>
</div>
<footer class="text-center">
    <p>© Алиев Вусал 2020&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url();?>/index.php/pages/view/agreement">Пользовательское соглашение</a></p>
</footer>
</body>
</html>
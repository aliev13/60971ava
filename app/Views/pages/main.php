<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron">
        <div class="d-flex justify-content-center">
            <h1 class="display-4">Кулинарный справочник!</h1>
        </div>
        <div class="d-flex justify-content-center">
           <div class="container">
               <p class="lead text-center">
                   Это приложение создано для быстрого доступа ко всем популярным рецептам. Здесь можно просмотреть время, которое
                   потребуется для приготовления того или иного блюда, посмотреть рецепт и его ингридиенты.
               </p>
           </div>
        </div>
        <hr class="my-4">
        <div class="d-flex justify-content-center">
            <div class="mb-3">
                <img src="https://image.winudf.com/v2/image1/Y29tLnZva2lsYW0uc2VydmVzY2FsY19pY29uXzE1OTUxMzM4ODdfMDM3/icon.png?w=170&fakeurl=1" alt="">
            </div>
        </div>

    </div>
<?= $this->endSection() ?>
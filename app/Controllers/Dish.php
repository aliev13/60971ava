<?php
namespace App\Controllers;

use App\Models\DishModel;
use Aws\S3\S3Client;
use CodeIgniter\Controller;

class Dish extends BaseController

{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new DishModel();
        $data ['dish'] = $model->getDish();
        echo view('dish/allDish', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new DishModel();
        $data ['dish'] = $model->getDish($id);
        echo view('dish/oneDish', $this->withIon($data));
    }
    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'cooking_method'  => 'required',
                'ingredient'  => 'required',
                'cooking_time'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new DishModel();

            $data = [
                'name' => $this->request->getPost('name'),
                'cooking_method' => $this->request->getPost('cooking_method'),
                'cooking_time' => $this->request->getPost('cooking_time'),
                'ingredient' => $this->request->getPost('ingredient'),
            ];


            if (!is_null($insert))
                $data['photo'] = $insert['ObjectURL'];

            $model->save($data);

            session()->setFlashdata('message', lang('Dish.dish_create_success'));
            return redirect()->to('/dish');
        }
        else
        {
            return redirect()->to('/dish/create')->withInput();
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('dish/create', $this->withIon($data));
    }

    public function update()
    {
        helper(['form','url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[3]|max_length[255]',
                'cooking_method'  => 'required',
                'ingredient'  => 'required',
                'cooking_time'  => 'required',
                'photo'  => 'required',
            ]))
        {
            $model = new DishModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'cooking_method' => $this->request->getPost('cooking_method'),
                'ingredient' => $this->request->getPost('ingredient'),
                'cooking_time' => $this->request->getPost('cooking_time'),
                'photo' => $this->request->getPost('photo'),
            ]);
            session()->setFlashdata('message', lang('Dish.dish_update_success'));
            return redirect()->to('/dish');
        }
        else
        {
            return redirect()->to('/dish/edit/'.$this->request->getPost('id'))->withInput();
        }
    }


    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new DishModel();

        helper(['form']);
        $data ['dish'] = $model->getDish($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('dish/edit', $this->withIon($data));

    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new DishModel();
        $model->delete($id);
        return redirect()->to('/dish');
    }
}
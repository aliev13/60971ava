<?php
namespace App\Models;
use CodeIgniter\Model;
class DishModel extends Model
{
    protected $table = 'dish'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'cooking_method', 'cooking_time', 'category_id', 'photo', 'ingredient'];
    public function getDish($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
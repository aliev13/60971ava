<?php
namespace App\Models;
use CodeIgniter\Model;
class RecipeModel extends Model
{
    protected $table = 'recipe'; //таблица, связанная с моделью
    protected $allowedFields = ['dish_id', 'ingredient_id', 'count'];
    public function getRecipe($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
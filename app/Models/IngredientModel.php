<?php
namespace App\Models;
use CodeIgniter\Model;
class IngredientModel extends Model
{
    protected $table = 'ingredient'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'units'];
    public function getIngredient($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}